# Alert CLI Utility for Linux

This program simplifies the process of sending automated alerts, and defines some standard environment variables used for that purpose.

## Installation

Simply drop the `alert` file into a location on your system `$PATH`.

It is recommended that you also define some environment variables before using this program.  You can use the program without defining these variables, but without them you will be required to use opt flags to provide methods for sending alerts.

- `ALERT_EMAIL`: Defines a default address where alerts can be emailed.
- `ALERT_SLACK`: Defines a default Slack webhook where alerts can be posted.
- `ALERT_PREF`: Defines which method(s) should be used for default alerts; can be "email", "slack", or both (use spaces to define multiple options: e.g.: "email slack").

## Usage

```
alert [OPTIONS] MESSAGE
```

Run `alert --help` for a full list of options.